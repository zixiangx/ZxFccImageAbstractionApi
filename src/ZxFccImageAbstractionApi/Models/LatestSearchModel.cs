﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccImageAbstractionApi.Models
{
    public class LatestSearchModel
    {
        public string term { get; set; }
        public DateTime datetime { get; set; }
    }
}
