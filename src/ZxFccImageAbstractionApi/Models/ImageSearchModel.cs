﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccImageAbstractionApi.Models
{
    public class ImageSearchModel
    {
        public string Url { get; set; }
        public string Snippet { get; set; }
    }
}
