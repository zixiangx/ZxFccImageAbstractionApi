﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccImageAbstractionApi.Models
{
    public class LatestSearch
    {
        public ObjectId _id { get; set; }
        public string term { get; set; }
        public DateTime datetime { get; set; }
    }
}
