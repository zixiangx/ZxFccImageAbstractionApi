﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZxFccImageAbstractionApi.Interfaces;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Bson;

namespace ZxFccImageAbstractionApi.Models
{
    public class MongoDbContext : IMongoDbContext
    {
        MongoClient Client;
        IMongoDatabase Database;

        public MongoDbContext(IOptions<MongoSettings> mongoSettings)
        {
            Client = new MongoClient(mongoSettings.Value.ConnectionString);
            Database = Client.GetDatabase(mongoSettings.Value.DatabaseName);
        }

        public IMongoCollection<LatestSearch> LatestSearch
        {
            get
            {
                return Database.GetCollection<LatestSearch>("LatestSearch");
            }
        }
    }
}
