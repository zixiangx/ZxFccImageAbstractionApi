﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccImageAbstractionApi.Models
{
    public class ImgurSettings
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Username { get; set; }
        public int AccountId { get; set; }
        public string TokenType { get; set; }
    }
}
