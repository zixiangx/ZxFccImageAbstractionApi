﻿using ZxFccImageAbstractionApi.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccImageAbstractionApi.Interfaces
{
    public interface IMongoDbContext
    {
        IMongoCollection<LatestSearch> LatestSearch { get; }
    }
}
