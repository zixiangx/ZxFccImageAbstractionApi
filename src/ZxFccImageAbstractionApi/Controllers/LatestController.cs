﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZxFccImageAbstractionApi.Interfaces;
using MongoDB.Driver;
using ZxFccImageAbstractionApi.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ZxFccImageAbstractionApi.Controllers
{
    [Route("api/[controller]")]
    public class LatestController : Controller
    {
        protected readonly IMongoDbContext _db;
        public LatestController(IMongoDbContext db)
        {
            _db = db;
        }
        
        [HttpGet]
        public List<LatestSearchModel> Get()
        {
            const string KEY_SORT = "datetime";

            FilterDefinition<LatestSearch> filter = Builders<LatestSearch>.Filter.Empty;
            SortDefinition<LatestSearch> sort = Builders<LatestSearch>.Sort.Descending(KEY_SORT);

            List<LatestSearch> result = _db.LatestSearch.Find(filter).Limit(10).Sort(sort).ToList();

            return MapToLatestSearchModel(result);
        }

        #region Private Utilities
        private List<LatestSearchModel> MapToLatestSearchModel(List<LatestSearch> input)
        {
            List<LatestSearchModel> model = new List<LatestSearchModel>();

            foreach (LatestSearch k in input)
            {
                model.Add(new LatestSearchModel
                {
                    term = k.term,
                    datetime = k.datetime
                });
            }

            return model;
        }
        #endregion
    }
}
