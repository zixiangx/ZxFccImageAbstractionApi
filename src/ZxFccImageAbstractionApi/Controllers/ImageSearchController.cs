﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.Extensions.Options;
using ZxFccImageAbstractionApi.Models;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZxFccImageAbstractionApi.Interfaces;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ZxFccImageAbstractionApi.Controllers
{
    [Route("api/[controller]")]
    public class ImageSearchController : Controller
    {
        protected readonly ImgurSettings _imgurSettings;
        protected readonly IMongoDbContext _db;
        private HttpClient client = new HttpClient();

        
        public ImageSearchController(IOptions<ImgurSettings> imgurSettings, IMongoDbContext db)
        {
            _imgurSettings = imgurSettings.Value;
            _db = db;
        }
        
        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("{searchString}")]
        public string Get(string searchString)
        {
            int offset;
            int.TryParse(Request.Query["offset"], out offset);

            _db.LatestSearch.InsertOne(new LatestSearch()
            {
                term = searchString,
                datetime = DateTime.UtcNow
            });

            string resultString = GetResultFromImgur(searchString);
            List<ImageSearchModel> modelResult = MapToImageSearchModel(resultString, offset);

            return JsonConvert.SerializeObject(modelResult);
        }

        private string GetResultFromImgur(string searchString)
        {
            string targetUrl = $"https://api.imgur.com/3/gallery/search?q_any={searchString}";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(_imgurSettings.TokenType, _imgurSettings.AccessToken);

            Task<HttpResponseMessage> response = client.GetAsync(targetUrl);

            return response.Result.Content.ReadAsStringAsync().Result;
        }

        private List<ImageSearchModel> MapToImageSearchModel(string jsonString, int offset)
        {
            const string JSON_KEY_DATA = "data";
            const string JSON_KEY_LINK = "link";
            const string JSON_KEY_TITLE = "title";

            int currentOffset = 0;
            List<ImageSearchModel> modelList = new List<ImageSearchModel>();
            JToken json = JObject.Parse(jsonString)[JSON_KEY_DATA];

            foreach (var item in json)
            {
                if (offset != 0 && currentOffset >= offset)
                    break;

                modelList.Add(new ImageSearchModel()
                {
                    Url = item[JSON_KEY_LINK].ToString(),
                    Snippet = item[JSON_KEY_TITLE].ToString()
                });

                currentOffset++;
            }

            return modelList;
        }
    }
}
