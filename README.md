This is an implementation of a Image Abstraction Layer that wraps around
an existing image search API to allow customized api endpoints.

In this example, the Imgur API is used as the underlying API, and the .NET
Core implementation allows for search to be passed through to the API, as well
as storage of the latest search terms in a Mongo DB Database.

The demo can be accessed via:

http://zxfccimageabstraction.azurewebsites.net